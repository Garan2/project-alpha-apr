from django.db import models
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        login_required(User),
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )
    # if User is not None:

    #     #backend authenticated
    # else:
    #     # No backend authenticated
    def __str__(self):
        return self.name
