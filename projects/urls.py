from projects.views import Project_list, Project_details
from django.urls import path


urlpatterns = [
    path("projects/", Project_list, name="list_projects"),
    path("projects/<int:id>/", Project_details, name="show_project")
]
