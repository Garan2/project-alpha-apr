from django.shortcuts import render, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required


# from django.shortcuts import redirect
# Create your views here.


@login_required
def Project_list(request):
    Project_list = Project.objects.filter(owner=request.user)
    context = {
        "project_list": Project_list,
    }

    return render(request, "projects/list.html", context)


@login_required
def Project_details(request, id):
    project_details = get_object_or_404(Project, id=id)
    context = {"project_details": project_details}

    return render(request, "projects/detail.html", context)


# shows project details
